<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bulma.css">
    <link rel="stylesheet" href="css/myCss.css">
    <script src="js/jquery.min.js"></script>
    <title>Connexion</title>
</head>
<body>
<script>
    function hide(){
        $(".delete").parent('p').fadeOut();
    };
</script>
    <div class="container">
        <br>
        <?php include 'php/moduleAlert.php' ?>
        <div class="column is-4 is-offset-4" style="padding-top: 10vh;">
            <div class="card" style="background-color: rgba(236, 240, 241,0.8);">
                <header class="card-header">
                    <p class="card-header-title">
                        AMADOU MARKET - Connexion

                    </p>

                </header>
                <div class="card-content">
                    <form action="php/moduleAuthentification.php" method="post">
                        <div class="field">
                            <label class="label">Identifiant</label>
                            <div class="control">
                                <input class="input" type="text" name="login" ">
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Mot de passe</label>
                            <div class="control">
                                <input class="input" type="password" name="password" >
                            </div>
                        </div>
                        <div class="field">
                            <label class="label">Profil</label>
                            <div class="control">
                                <div class="select">
                                    <select name="profil">
                                        <option value="admin">Administrateur</option>
                                        <option value="user">Utilisateur</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control">
                            <button type="submit" name="connect" class="button is-primary">Se Connecter</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



</body>
</html>