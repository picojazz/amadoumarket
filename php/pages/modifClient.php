<?php
if(isset($_GET["supp"]) && $_GET["id"] !="ok"){
    $id=$_GET["id"];
    $req=$db->prepare("DELETE FROM clients  WHERE codecli=?");
    $req->execute(array($id)) or header("location:admin.php?p=listeClientsClient&status=errorSql");
    header("location:admin.php?p=listeClients&status=suppOk");

}
    if(isset($_GET["id"]) && $_GET["id"] !="") {
    $req = $db->prepare("SELECT * FROM clients WHERE codecli = ?");
    $req->execute(array($_GET["id"]));
    $res = $req->fetch(PDO::FETCH_OBJ);
    }
if(isset($_POST["modifier"])){
    if((isset($_POST["nom"]) && $_POST["nom"] !="") && (isset($_POST["prenom"]) && $_POST["prenom"] !="") && (isset($_POST["tel"]) && $_POST["tel"] !="") && (isset($_POST["adresse"]) && $_POST["adresse"] !="")){
        $nom=$_POST["nom"];
        $prenom=$_POST["prenom"];
        $tel=$_POST["tel"];
        $type=$_POST["type"];
        $adresse=$_POST["adresse"];
        $id=$_POST["id"];



        $req=$db->prepare("UPDATE clients SET nom=?,prenom=?,tel=?,type=?,adresse=? WHERE codecli=?");
        $req->execute(array($nom,$prenom,$tel,$type,$adresse,$id)) or header("location:admin.php?p=modifClient&status=errorSql&id=".$_GET['id']);
        header("location:admin.php?p=listeClients&status=modifOk");


    }else{
        header("location:admin.php?p=modifClient&status=error&id=".$_GET['id']);
    }
}
?>

<div class="column is-8 is-offset-2">
    <div class="card" style="background-color: rgba(255, 255, 255,0.9);">
        <header class="card-header">
            <p class="card-header-title title" style="color: rgba(41, 128, 185,1.0);">Modification d'un Client</p>
        </header>
        <div class="card-content">
            <form action="admin.php?p=modifClient&id=<?php echo $res->codecli; ?>" method="post">
                <div class="columns">
                    <div class="column is-half">
                        <input type="hidden" name="id" value="<?php echo $res->codecli; ?>">
                        <div class="field">
                            <label>Nom</label>
                            <div class="control">
                                <input class="input" type="text" name="nom" value="<?php echo $res->nom ; ?>">
                            </div>
                        </div>
                        <div class="field">
                            <label>Prenom</label>
                            <div class="control">
                                <input class="input" type="text" name="prenom" value="<?php echo $res->prenom ; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <label>Telephone</label>
                        <div class="field has-addons">

                            <p class="control">
                                <a class="button is-static">+221</a>
                            </p>
                            <div class="control">
                                <input class="input" type="text" name="tel" value="<?php echo $res->tel ; ?>">
                            </div>
                        </div>
                        <div class="field">
                            <label>Type</label>
                            <div class="control">
                                <div class="select">
                                    <select name="type" >
                                        <option value="<?php echo $res->type ; ?>"><?php echo $res->type ; ?></option>
                                        <?php if($res->type == 'entreprise'){ ?>
                                        <option value="personne">personne</option>
                                        <?php }else{ ?>
                                        <option value="entreprise">entreprise</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label>Adresse</label>
                            <div class="control">
                                <textarea name="adresse" rows="2" class="textarea" ><?php echo $res->adresse ; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="field is-grouped">
                    <p class="control">
                        <button type="submit" name="modifier" class="button is-success">Modifier</button>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>