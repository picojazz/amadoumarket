<?php

if(isset($_POST["ajout"])){
    if((isset($_POST["nom"]) && $_POST["nom"] !="") && (isset($_POST["prenom"]) && $_POST["prenom"] !="") && (isset($_POST["tel"]) && $_POST["tel"] !="") && (isset($_POST["adresse"]) && $_POST["adresse"] !="")){
        $nom=$_POST["nom"];
        $prenom=$_POST["prenom"];
        $tel=$_POST["tel"];
        $type=$_POST["type"];
        $adresse=$_POST["adresse"];



            $req=$db->prepare("INSERT INTO clients (nom,prenom,tel,type,adresse)VALUES(?,?,?,?,?)");
            $req->execute(array($nom,$prenom,$tel,$type,$adresse)) or header("location:admin.php?p=ajoutClient&status=errorSql");
            header("location:admin.php?p=ajoutClient&status=ajoutOk");


    }else{
        header("location:admin.php?p=ajoutClient&status=error");
    }
}

?>

<div class="column is-8 is-offset-2">
    <div class="card" style="background-color: rgba(255, 255, 255,0.9);">
        <header class="card-header">
            <p class="card-header-title title" style="color: rgba(41, 128, 185,1.0);">Ajout d'un Client</p>
        </header>
        <div class="card-content">
            <form action="admin.php?p=ajoutClient" method="post">
                <div class="columns">
                    <div class="column is-half">
                        <div class="field">
                            <label>Nom</label>
                            <div class="control">
                                <input class="input" type="text" name="nom" placeholder="Dieng">
                            </div>
                        </div>
                        <div class="field">
                            <label>Prenom</label>
                            <div class="control">
                                <input class="input" type="text" name="prenom" placeholder="Amadaou">
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <label>Telephone</label>
                        <div class="field has-addons">

                            <p class="control">
                                <a class="button is-static">+221</a>
                            </p>
                            <div class="control">
                                <input class="input" type="text" name="tel" placeholder="77-951-86-54">
                            </div>
                        </div>
                        <div class="field">
                            <label>Type</label>
                            <div class="control">
                                <div class="select">
                                    <select name="type">
                                        <option value="entreprise">Entreprise</option>
                                        <option value="personne">Personne</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <label>Adresse</label>
                            <div class="control">
                                <textarea name="adresse" rows="2" class="textarea" placeholder="Ouakam,cité assemblée villa n°164"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="field is-grouped">
                    <p class="control">
                        <button type="submit" name="ajout" class="button is-info">Ajouter</button>
                    </p>
                </div>
            </form>
        </div>
    </div>
</div>