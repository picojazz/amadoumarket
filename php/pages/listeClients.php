<?php
    $req2 = $db->query("SELECT * FROM clients");
    $nbAll=$req2->rowCount();
    $nPage = 8;
    $page = ceil($nbAll/$nPage);
    if (isset($_GET['page']) && $_GET['page'] > 0 && $_GET['page'] <= $page) {
    $pageActuel = $_GET['page'] ;
    }else{
    $pageActuel = 1 ;
    }
    $req = $db->query("SELECT * FROM clients ORDER BY codecli DESC LIMIT ".(($pageActuel - 1)*$nPage).",$nPage");

?>

<div class="column is-10 is-offset-1">
    <div class="card">
        <header class="card-header">
            <p class="card-header-title title" style="color: rgba(41, 128, 185,1.0);">Liste des Clients</p>
        </header>
        <div class="card-content has-text-centered">
            <table class="table is-striped is-hoverable">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Telephone</th>
                    <th>Type</th>
                    <th>Adresse</th>
                    <th>Modifier</th>
                    <th>Supprimer</th>
                </tr>
                </thead>
                <tbody>
                <?php while($res=$req->fetch(PDO::FETCH_OBJ)){ ?>
                <tr>
                    <td><?php echo $res->codecli ; ?></td>
                    <td><?php echo $res->nom ; ?></td>
                    <td><?php echo $res->prenom ; ?></td>
                    <td><?php echo $res->tel ; ?></td>
                    <td><?php echo $res->type ; ?></td>
                    <td><?php echo $res->adresse ; ?></td>
                    <td><a href="admin.php?p=modifClient&id=<?php echo $res->codecli ; ?>" class="button is-primary">modifier</a></td>
                    <td><a href="admin.php?p=modifClient&supp=ok&id=<?php echo $res->codecli ; ?>" class="button red">supprimer</a></td>
                </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="colums">
    <nav class="pagination column " role="navigation" aria-label="pagination">

        <ul class="pagination-list">
            <?php for ($i=1; $i<=$page ; $i++) {  ?>
            <?php if ($i == $pageActuel){ ?>
            <li>
                <a class="pagination-link is-current"  aria-current="page" href="admin.php?p=listeClients&page=<?php echo $i ?>"><?php echo $i ?></a>
            </li>
            <?php }else{ ?>
            <li>
                <a class="pagination-link" style="color: white;" href="admin.php?p=listeClients&page=<?php echo $i ?>"><?php echo $i ?></a>
            </li>
            <?php } } ?>

        </ul>
    </nav>
</div>