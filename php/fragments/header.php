<?php include 'moduleTestUser.php';?>
<?php include 'moduleConnexion.php';?>
    <!doctype html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="../css/bulma.css">
        <link rel="stylesheet" href="../css/myCss.css">
        <script src="../js/jquery.min.js"></script>
        <title>Document</title>
    </head>
    <body>
    <script>
        function hide(){
            $(".delete").parent('p').fadeOut();
        };
    </script>
    <nav class="navbar is-transparent" style="background-color: rgba(255, 255, 255,0.95);">
        <div class="navbar-brand">
            <a href="admin.php" class="navbar-item" style="font-weight: bolder; color: rgba(41, 128, 185,1.0);font-family: 'amadou Market';font-size: 25px;">
               AMADOU MARKET
            </a>
        </div>
        <div id="navbarExampleTransparentExample" class="navbar-menu">
            <div class="navbar-end">
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">Clients</a>
                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="admin.php?p=ajoutClient">Ajouter un Client</a>
                        <a class="navbar-item" href="admin.php?p=listeClients">Liste des Cllents</a>
                    </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">Fournisseurs</a>
                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="#">Ajouter un Fournisseur</a>
                        <a class="navbar-item" href="#">Liste des fournisseurs</a>
                    </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">Stocks</a>
                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="#">Ajouter un Produit</a>
                        <a class="navbar-item" href="#">Liste des Produits</a>
                    </div>
                </div>

                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">Commandes</a>
                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="#">faire une commande</a>
                        <a class="navbar-item" href="#">Liste des commandes</a>
                    </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">Fournitures</a>
                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="#">Mise a jour du stock</a>
                        <a class="navbar-item" href="#">Liste des fournitures</a>
                    </div>
                </div>
                <div class="navbar-item has-dropdown is-hoverable">
                    <a class="navbar-link" href="#">Livraisons</a>
                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="#">Livraisons en cours</a>
                        <a class="navbar-item" href="#">Liste des livraisons effectuées</a>
                    </div>
                </div>
                <a class="navbar-item" href="#">Factures</a>
                <a class="navbar-item" href="moduleAuthentification.php?erreur=logout" style="font-weight: bolder; color: rgba(41, 128, 185,1.0);">Se deconnecter</a>
            </div>
        </div>
    </nav>


        <div class="container" style="padding-bottom: 80px;">
            <br>
<?php include 'moduleAlert.php'?>
